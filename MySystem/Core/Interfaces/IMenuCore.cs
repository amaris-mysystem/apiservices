﻿
using MySystem.Models.BdMySystem;

namespace MySystem.Core.Interfaces
{
    public interface IMenuCore
    {
        public Menu GetOneMenuById(int idMenu);
        public List<Menu> GetManyMenu();
        public bool CreateMenu(Menu menu);
        public bool UpdateMenu(int id, Menu menu);
        public bool DeleteMenu(int idMenu);
    }
}
