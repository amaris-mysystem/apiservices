﻿using Microsoft.AspNetCore.Mvc;
using MySystem.Core;
using MySystem.Core.Interfaces;
using MySystem.Models.BdMySystem;

namespace MySystem.Controllers0
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly IMenuCore menuCore;
        public MenuController()
        {
            menuCore = new MenuCore();
        }
        // GET: api/MenuController/1
        [HttpGet("{idMenu}")]
        public Menu GetOneMenuById(int idMenu)
        {
            return menuCore.GetOneMenuById(idMenu);
        }

        // GET api/MenuController
        [HttpGet]
        public List<Menu> GetManyMenu()
        {
            return menuCore.GetManyMenu();
        }

        // POST api/MenuController
        [HttpPost]
        public bool CreateMenu(Menu menu)
        {
            return menuCore.CreateMenu(menu);
        }

        // PUT api/MenuController/1
        [HttpPut("{idMenu}")]
        public bool UpdateMenu(int idMenu, Menu menu)
        {
            return menuCore.UpdateMenu(idMenu, menu);
        }

        // DELETE api/MenuController/5
        [HttpDelete("{idMenu}")]
        public bool Delete(int idMenu)
        {
            return menuCore.DeleteMenu(idMenu);
        }
    }
}
