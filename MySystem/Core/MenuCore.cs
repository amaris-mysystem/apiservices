﻿using MySystem.Core.Interfaces;
using MySystem.Models.BdMySystem;

namespace MySystem.Core
{
    public class MenuCore : IMenuCore
    {
        private readonly BdmysystemContext bdMySystemContext;
        public MenuCore() 
        { 
            bdMySystemContext = new BdmysystemContext();
        }

        public MenuCore(BdmysystemContext bdMySystemContext)
        {
            this.bdMySystemContext = bdMySystemContext; 
        }

        public Menu GetOneMenuById(int idMenu)
        {
            return bdMySystemContext.Menus.Where(item => item.IdMenu == idMenu).First();
        }

        public List<Menu> GetManyMenu()
        { 
            return [.. bdMySystemContext.Menus.OrderBy(item => item.IdMenu)];
        }
        
        public bool CreateMenu(Menu menu)
        {
            bdMySystemContext.Menus.Add(menu);
            bdMySystemContext.SaveChanges();
            return true;
        }

        public bool UpdateMenu(int idMenu, Menu menu)
        {
            var menuUpdate = bdMySystemContext.Menus.Where(item => item.IdMenu == idMenu).FirstOrDefault();
            if(menuUpdate != null) 
            {
                menuUpdate.Name = menu.Name;
                menuUpdate.CookingRecipe = menu.CookingRecipe;
                menuUpdate.Calories = menu.Calories;
                bdMySystemContext.SaveChanges(true);
                return true;
            }
            return false;
        }
        public bool DeleteMenu(int idMenu)
        {
            var menu = bdMySystemContext.Menus.Where(item => item.IdMenu == idMenu).First();
            bdMySystemContext.Menus.Remove(menu);
            bdMySystemContext.SaveChanges();
            return true;
        }
    }
}
