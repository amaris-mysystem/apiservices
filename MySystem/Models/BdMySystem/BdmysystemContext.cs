﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace MySystem.Models.BdMySystem;

public partial class BdmysystemContext : DbContext
{
    public BdmysystemContext()
    {
    }

    public BdmysystemContext(DbContextOptions<BdmysystemContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Menu> Menus { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see https://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseMySQL("server=127.0.0.1;port=3306;database=bdmysystem;user=usrsystem;password=usrsystem;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Menu>(entity =>
        {
            entity.HasKey(e => e.IdMenu).HasName("PRIMARY");

            entity.ToTable("menu");

            entity.Property(e => e.IdMenu).HasColumnType("int(11)");
            entity.Property(e => e.Calories).HasColumnType("int(11)");
            entity.Property(e => e.CookingRecipe).HasColumnType("text");
            entity.Property(e => e.Name).HasMaxLength(100);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
