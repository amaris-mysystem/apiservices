﻿using System;
using System.Collections.Generic;

namespace MySystem.Models.BdMySystem;

public partial class Menu
{
    public int IdMenu { get; set; }

    public string Name { get; set; } = null!;

    public string CookingRecipe { get; set; } = null!;

    public int Calories { get; set; }
}
